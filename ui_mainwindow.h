/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QCustomPlot *widget;
    QSpinBox *spinBox;
    QPushButton *pushButton_GenSign;
    QSpinBox *spinBox_sampling;
    QSpinBox *spinBox_bitrate;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1006, 669);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        widget = new QCustomPlot(centralwidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(40, 30, 751, 221));
        spinBox = new QSpinBox(centralwidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(880, 50, 71, 22));
        spinBox->setMaximum(10000);
        spinBox->setValue(10);
        pushButton_GenSign = new QPushButton(centralwidget);
        pushButton_GenSign->setObjectName(QStringLiteral("pushButton_GenSign"));
        pushButton_GenSign->setGeometry(QRect(850, 160, 111, 23));
        spinBox_sampling = new QSpinBox(centralwidget);
        spinBox_sampling->setObjectName(QStringLiteral("spinBox_sampling"));
        spinBox_sampling->setGeometry(QRect(900, 80, 71, 22));
        spinBox_sampling->setMaximum(10000);
        spinBox_sampling->setValue(350);
        spinBox_bitrate = new QSpinBox(centralwidget);
        spinBox_bitrate->setObjectName(QStringLiteral("spinBox_bitrate"));
        spinBox_bitrate->setGeometry(QRect(900, 110, 71, 22));
        spinBox_bitrate->setMaximum(10000);
        spinBox_bitrate->setValue(8);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(830, 90, 47, 13));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(830, 110, 47, 13));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(820, 50, 61, 20));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1006, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        pushButton_GenSign->setText(QApplication::translate("MainWindow", "Generate Signal", 0));
        label->setText(QApplication::translate("MainWindow", "Sampling:", 0));
        label_2->setText(QApplication::translate("MainWindow", "Bitrate:", 0));
        label_3->setText(QApplication::translate("MainWindow", "Bits_count:", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
